import { StyleSheet, Text, View, Image, TextInput, ScrollView, FlatList } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Login from './screens/Login';
import Todos from './screens/Todos';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>

      <Todos />

      {/* <Login /> */}
      {/* <View style={[styles.box, { backgroundColor: "salmon", alignItems: "stretch" }]}></View>
      
      <View style={[styles.box, { backgroundColor: "#fff678"}]}></View>

      <View style={[styles.box, { backgroundColor: "blue"}]}></View> */}
      
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  box: {
    width: 100,
    height: 100,
    backgroundColor: "black"
  }
});
