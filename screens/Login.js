import { View, Text, StyleSheet, ImageBackground, Image, TextInput, Button, TouchableOpacity } from 'react-native'
import React from 'react'

export default function Login() {
  return (
    <ImageBackground style={[styles.background]} source={require("../assets/avenger-background.jpeg")}>
        <Image style={styles.logo} source={require("../assets/avengers-logo.png")}/>

        <View style={styles.loginArea}>
            <TextInput style={styles.inputStyle} placeholder="Username"/>
            <TextInput style={styles.inputStyle} placeholder="Password"/>
        </View>

        <TouchableOpacity style={styles.button}>
            <Text>Register</Text>
        </TouchableOpacity>
        <Text style={styles.textStyle}>Don't have account yet? 
            <TouchableOpacity><Text style={{ color: "white"}}> Sign Up</Text></TouchableOpacity></Text>
        

    </ImageBackground>
  )
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "center"
    },
    logo: {
        width: 100,
        height: 100,
        top: 50,
        alignSelf: "center",
    },
    loginArea: {
        alignItems: "center",
        top: 200,
    },
    inputStyle: {
        width: 200,
        borderRadius: 10,
        margin: 10,
        padding: 5,
        backgroundColor: 'rgba(255,255,255, 0.3)'
    },
    button: {
        height: 40,
        width: "40%",
        borderRadius: 30,
        justifyContent: "center",
        alignItems:"center",
        alignSelf: "center",
        backgroundColor: "white",
        marginBottom: 70,
        bottom: 50,
        top: 200
    },
    textStyle: {
        top: 150,
        color: "white"
    }
})
