import { View, Text, FlatList } from 'react-native'
import React, { useState , useEffect} from 'react'
import Todo from '../components/Todo';

import { instance as axios}  from '../util/api';


export default function Todos() {

  const [ todos, setTodos ] = useState([]);

  // cara menggunakan async await
  const fetchTodos = async () => {
    try {
        const { data, status } = await axios.get("/todos");
        if (status === 200) {
            setTodos(data);
        }
    } catch(err) {
        console.log(err);
    }
  }
  
  useEffect(() => {
    fetchTodos();


    // ini menggunakan promise based
    // axios.get(`https://jsonplaceholder.typicode.com/todos`)
    // .then(({ data, status }) => {
    //     setTodos(data);
    // })
    // .catch(err => {
    //     console.log(err);
    // })
  }, []);


  return (
    <View>
      <FlatList 
        data={todos}
        renderItem={({item}) => <Todo item={item} setTodos={setTodos}/>}
      />
    </View>
  )
}