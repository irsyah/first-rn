import { View, Text, TouchableOpacity } from 'react-native'
import React from 'react'

export default function Todo({item, setTodos}) {

  const removeTodo = () => {
    console.log("start remove");
    setTodos(state => {
        return state.filter(todo => {
            if (todo.id !== item.id) {
                return todo
            }
        })
    })
  }

  return (
    <View>
      <Text>{item.title}</Text>
      <TouchableOpacity onPress={removeTodo}>
        <Text>Delete</Text>
      </TouchableOpacity>
    </View>
  )
}